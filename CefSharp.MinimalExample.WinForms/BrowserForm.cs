﻿// Copyright © 2010-2015 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using CefSharp.DevTools.IO;
using CefSharp.MinimalExample.WinForms.Controls;
using CefSharp.WinForms;
using System;
using System.Windows.Forms;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net.WebSockets;
using System.Diagnostics;

namespace CefSharp.MinimalExample.WinForms
{
    public partial class BrowserForm : Form
    {
#if DEBUG
        private const string Build = "Debug";
#else
        private const string Build = "Release";
#endif
        private readonly string title = "CefSharp.MinimalExample.WinForms (" + Build + ")";
        //private readonly ChromiumWebBrowser browser;
        public ChromiumWebBrowser chromeBrowser;
        public BrowserForm()
        {
            InitializeComponent();
            InitializeChromiumAsync();

        }
        public async Task InitializeChromiumAsync()
        {

            //CefSettings settings = new CefSettings();
            //settings.CefCommandLineArgs.Add("enable-media-stream", "1");

            //Cef.Initialize(settings);

            chromeBrowser = new ChromiumWebBrowser("https://face.deelsdf.xyz/ecat");
            //chromeBrowser.SetBounds(2000, 0, 800, 600);
            // Add it to the form and fill it to the form window.
            //Screen[] screens = Screen.AllScreens;
            //Rectangle bounds = screens[1].Bounds;
            //chromeBrowser.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            
            
        }
        
        private void BrowserForm_Layout(object sender, System.Windows.Forms.LayoutEventArgs e)
        {
            Screen[] screens = Screen.AllScreens;
            Rectangle bounds = screens[1].Bounds;
            Debug.WriteLine("" + bounds.X + " " + bounds.Y + " " + bounds.Width + " " + bounds.Height);
            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
        }
        

        private  void BrowserForm_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
        }
        private void BrowserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }
    }
}
