﻿using CefSharp.WinForms;
using System;
using System.Windows.Forms;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net.WebSockets;
using System.Diagnostics;

namespace CefSharp.MinimalExample.WinForms
{
    public partial class MainForm : Form
    {
        public ChromiumWebBrowser chromeBrowser;

        public MainForm()
        {
            InitializeComponent();
            InitializeChromiumAsync();
        }
        public async Task InitializeChromiumAsync()
        {
            //CefSettings settings = new CefSettings { RemoteDebuggingPort = 8088 };

            //// Initialize cef with a command line argument
            //// In this case the enable-media-stream flag that allows you to access the camera and the microphone
            //settings.CefCommandLineArgs.Add("enable-media-stream", "1");

            //Cef.Initialize(settings);
            chromeBrowser = new ChromiumWebBrowser("https://face.deelsdf.xyz/hung-ecat");
            //chromeBrowser.SetBounds(2000, 0, 800, 600);
            // Add it to the form and fill it to the form window.
            Screen[] screens = Screen.AllScreens;
            Rectangle bounds = screens[0].Bounds;
            chromeBrowser.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            ClientWebSocket webSocket = new ClientWebSocket();

            await webSocket.ConnectAsync(new Uri("ws://127.0.0.1:8081"), CancellationToken.None);

            string message = "Hello World";
            Console.WriteLine("Sending message: " + message);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            byte[] incomingData = new byte[1024];
            await webSocket.SendAsync(new ArraySegment<byte>(messageBytes), WebSocketMessageType.Text, true, CancellationToken.None);
            BrowserForm browserForm = new BrowserForm();
            browserForm.Top = 0;
            browserForm.Left = Screen.AllScreens[1].WorkingArea.Left;
            while (true)
            {

                WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(incomingData), CancellationToken.None);
                if (result.CloseStatus.HasValue)
                {
                    Console.WriteLine("Closed; Status: " + result.CloseStatus + ", " + result.CloseStatusDescription);
                    Debug.WriteLine("Closed; Status: " + result.CloseStatus + ", " + result.CloseStatusDescription);

                }
                else
                {
                    

                    Console.WriteLine("Received message: " + Encoding.UTF8.GetString(incomingData, 0, result.Count));
                    Debug.WriteLine("Received message: " + Encoding.UTF8.GetString(incomingData, 0, result.Count));
                    if (Encoding.UTF8.GetString(incomingData, 0, result.Count).Equals("open"))
                    {

                        browserForm.Show();
                        browserForm.WindowState = FormWindowState.Maximized;

                        //this.Dispose();
                    }
                    else browserForm.Dispose();
                }
            }
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
