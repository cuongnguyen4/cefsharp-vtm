﻿namespace CefSharp.MinimalExample.WinForms
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
            this.SuspendLayout();
            // 
            // Form1
            // 

            //this.SetBounds(2000, 0, 800, 600);
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Layout += (this.BrowserForm_Layout);
            this.Load += new System.EventHandler(this.BrowserForm_Load);
            this.ResumeLayout(false);
        }

        #endregion

  //      private System.Windows.Forms.ToolStripContainer toolStripContainer;
  //      private System.Windows.Forms.ToolStrip toolStrip1;
  //      private System.Windows.Forms.ToolStripButton backButton;
  //      private System.Windows.Forms.ToolStripButton forwardButton;
  //      private System.Windows.Forms.ToolStripTextBox urlTextBox;
  //      private System.Windows.Forms.ToolStripButton goButton;
  //      private System.Windows.Forms.MenuStrip menuStrip1;
  //      private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
  //      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
  //      private System.Windows.Forms.Label outputLabel;
  //      private System.Windows.Forms.Label statusLabel;
		//private System.Windows.Forms.ToolStripMenuItem showDevToolsToolStripMenuItem;

    }
}